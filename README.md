# Nostalgia dokuwiki template

* Based on the default and only theme for UseModWiki, that got [converted to Mediawiki](https://www.mediawiki.org/wiki/Skin:Nostalgia)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:nostalgia)

![newspaper theme screenshot](https://i.imgur.com/2tPuXTD.png)